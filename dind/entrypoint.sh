#!/usr/bin/env sh

addgroup --gid ${DOCKER_GID} docker

exec dockerd-entrypoint.sh dockerd --host="unix:///var/run/docker/docker.sock"
