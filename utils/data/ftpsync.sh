#!/usr/bin/env bash

set -e

hash lftp

declare host user password localeDirectory remoteDirectory

usage() {
  echo "usage: $0 -h 127.0.0.1 -u test -p 1234 -l /home/test/ -r /remote/" >&2
  exit 1
}

if [ "$*" == '' ]; then
  usage
fi

while getopts h:u:p:l:r: flag; do
  case "${flag}" in
  h) host="${OPTARG}" ;;
  u) user="${OPTARG}" ;;
  p) password="${OPTARG}" ;;
  l) localeDirectory="${OPTARG}" ;;
  r) remoteDirectory="${OPTARG}" ;;
  *) usage ;;
  esac
done

lftp -c "set ftp:list-options -a;
open 'ftp://$user:$password@$host';
lcd $localeDirectory;
cd $remoteDirectory;
mirror \
       --delete \
       --verbose \
       --parallel=10 \
       --exclude-glob a-dir-to-exclude/ \
       --exclude-glob a-file-to-exclude \
       --exclude-glob a-file-group-to-exclude* \
       --exclude-glob other-files-to-exclude"
